
function validarNouArticle() {
    var titol = $("#titol").val();
    var icoTitol = $("#icoTitolArticle");
    var titolOk = false;

    var cos = $("#cosArticle").val();
    var icoCos = $("#icoCosArticle");
    var cosOk = false;

    var idioma = $("#idiomaArticle").val();
    var icoIdioma = $("#icoIdiomaArticle");
    var idiomaOk = false;

    var data = $("#dataArticle").val();
    var icoData = $("#icoDataArticle");
    var dataOk = false;

    var submit = $("#submitArticle");



    if (titol.length < 5) {
        titolOk = false;
        icoTitol.addClass("fa-times");
        icoTitol.css("color", "red");
        icoTitol.removeClass("fa-check");
    } else {
        titolOk = true;
        icoTitol.addClass("fa-check");
        icoTitol.css("color", "green");
        icoTitol.removeClass("fa-times");
    }


    if (cos.length < 35) {
        cosOk = false;
        icoCos.addClass("fa-times");
        icoCos.css("color", "red");
        icoCos.removeClass("fa-check");
    } else {
        cosOk = true;
        icoCos.addClass("fa-check");
        icoCos.css("color", "green");
        icoCos.removeClass("fa-times");
    }


    if (idioma === "") {
        idiomaOk = false;
        icoIdioma.addClass("fa-times");
        icoIdioma.css("color", "red");
        icoIdioma.removeClass("fa-check");
    } else {
        idiomaOk = true;
        icoIdioma.addClass("fa-check");
        icoIdioma.css("color", "green");
        icoIdioma.removeClass("fa-times");
    }


    if (data === "") {
        dataOk = false;
        icoData.addClass("fa-times");
        icoData.css("color", "red");
        icoData.removeClass("fa-check");
    } else {
        dataOk = true;
        icoData.addClass("fa-check");
        icoData.css("color", "green");
        icoData.removeClass("fa-times");
    }



    if (titolOk && dataOk && idiomaOk)
        submit.attr("disabled", false);

    else
        submit.attr("disabled", true);

}


function validarNouEsdeveniment() {

    /*
    ####################
    #      ITEMS       #
    ####################
     */
    var titol = $("#titolEsdeveniment").val();
    var icoTitol = $("#icoTitolEsdeveniment");
    var titolOK = false;

    var data = $("#dataEsdeveniment").val();
    var icoData = $("#icoDataEsdeveniment");
    var dataOK = false;

    var hora = $("#horaEsdeveniment").val();
    var icoHora = $("#icoHoraEsdeveniment");
    var horaOK = false;

    var nomLloc = $("#nomLlocEsdeveniment").val();
    var icoLloc = $("#icoLlocEsdeveniment");
    var nomOK = false;

    var teWeb = $("#teWeb:checked").val();
    var teWebElLocal = false;

    var direccioWeb = $("#paginaWebUrl").val();
    var icoWeb = $("#icoWeb");
    var webOK = false;

    var direccioCarrer = $("#carrerEsveveniment").val();
    var icoCarrer = $("#icoCarrerEsdeveniment");
    var carrerOK = false;

    var submit = $("#afegirEsdeveniment");



    // Mostrar o ocultar textbox per a afegir la URL de la pagina web del local
    if (teWeb === "true"){
        $("#paginaWebUrl").css("display", "inline");
        $("#labelWeb").css("display", "inline");
    } else {
        $("#paginaWebUrl").css("display", "none");
        $("#labelWeb").css("display", "none");

    }


    /*
    #########################
    #       VALIDACIONS     #
    #########################
     */

    if (titol === "" || titol === undefined) {
        titolOK = false;
        icoTitol.addClass("fa-times");
        icoTitol.css("color", "red");
        icoTitol.removeClass("fa-check");
    }
    else {
        titolOK = true;
        icoTitol.addClass("fa-check");
        icoTitol.css("color", "green");
        icoTitol.removeClass("fa-times");
    }


    if (data === null || data === "" || data === undefined) {
        dataOK = false;
        icoData.addClass("fa-times");
        icoData.css("color", "red");
        icoData.removeClass("fa-check");
    }
    else {
        dataOK = true;
        icoData.addClass("fa-check");
        icoData.css("color", "green");
        icoData.removeClass("fa-times");
    }


    if (hora === null < 4 || hora === "" || hora === undefined) {
        horaOK = false;
        icoHora.addClass("fa-times");
        icoHora.css("color", "red");
        icoHora.removeClass("fa-check");
    }
    else {
        horaOK = true;
        icoHora.addClass("fa-check");
        icoHora.css("color", "green");
        icoHora.removeClass("fa-times");
    }


    if (nomLloc === "" || nomLloc === " ") {
        nomOK = false;
        icoLloc.addClass("fa-times");
        icoLloc.css("color", "red");
        icoLloc.removeClass("fa-check");
    }
    else {
        nomOK = true;
        icoLloc.addClass("fa-check");
        icoLloc.css("color", "green");
        icoLloc.removeClass("fa-times");
    }


    if (teWeb === "true") {
        teWebElLocal = true;

        if (direccioWeb === "" || direccioWeb === " ") {
            webOK = false;
            icoWeb.addClass("fa-times");
            icoWeb.css("color", "red");
            icoWeb.removeClass("fa-check");
        }
        else {
            webOK = true;
            icoWeb.addClass("fa-check");
            icoWeb.css("color", "green");
            icoWeb.removeClass("fa-times");
        }
    }
    else {
        teWebElLocal = false;
        icoWeb.removeClass("fa-times");
        icoWeb.removeClass("fa-check");

    }


    if (direccioCarrer === "" || direccioCarrer === " ") {
        carrerOK = false;
        icoCarrer.addClass("fa-times");
        icoCarrer.css("color", "red");
        icoCarrer.removeClass("fa-check");
    }
    else {
        carrerOK = true;
        icoCarrer.addClass("fa-check");
        icoCarrer.css("color", "green");
        icoCarrer.removeClass("fa-times");
    }

    /*
    #################################
    #       ACTIVAR BOTÓ SUBMIT     #
    #################################
     */

    submit.attr("disabled", true);
    if (teWebElLocal){
        if (titolOK && dataOK && horaOK && nomOK && webOK && carrerOK)
            submit.attr("disabled", false);

        else
            submit.attr("disabled", true);
    } else {
        if (titolOK && dataOK && horaOK && nomOK && carrerOK)
            submit.attr("disabled", false);

        else
            submit.attr("disabled", true);
    }
}