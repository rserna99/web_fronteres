var root = null;
var useHash = true; // Defaults to: false
var hash = '#!'; // Defaults to: '#'
var router = new Navigo(root, useHash, hash);

router
    .on('/', mostrarIniciAdmin)
    .on('/afegir-articles', mostrarAfegirArticle)
    .on('/afegir-esdeveniments', mostrarAfegirEsdeveniment)
    .on('/modificar-articles', mostrarLlistaArticles)
    .on('/modificar-esdeveniments', mostrarModificarEsdeveniments)
    .resolve();

function mostrarIniciAdmin() {
    $("#app").html("<h1>Pagina d'administració</h1>");
}


function mostrarAfegirArticle() {
    var plantilla = $("#plantillaNouArticle").render();

    $("#app").html(plantilla);
}

function mostrarModificarArticles() {
    var plantilla = $("#plantillaModArticle").render();

    $("#app").html(plantilla);
}

function mostrarLlistaArticles() {

}

function mostrarAfegirEsdeveniment() {
    var plantilla = $("#plantillaNouEsdeveniment").render();

    $("#app").html(plantilla);
}

function mostrarModificarEsdeveniments() {
    var plantilla = $("#plantillaModEsdeveniment").render();

    $("#app").html(plantilla);
}

function mostrarLlistaArticles() {

    console.log($("#plantillaLlistaArticles").render());

    try {
        //$.getJSON("http://localhost:63342/web_fronteres/api/index.php/articles/cat", function(data){

            var plantilla = $("#plantillaLlistaArticles").render();

            console.log();

            $("#app").html(plantilla);

        //});
    } catch (e) {
        console.log(e)
    }
}