<?php

require("Slim/Slim.php");
require("../db/bd_connection.php");


\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();


$app->get('/',function() use ($app){
    $response = $app->response();
    $response['Content-Type'] = 'application/json';
    $response->status(404);

    return $response;
    }
)->setParams(array($app));



/* OBTENIR TOTS ELS ARTICLES EN CATALÀ */
$app->get('/articles/cat/',function() use ($app){
    $conexio = obrirConexio();

    $sql = "SELECT * FROM articles WHERE idioma_id=1";


    $resultat = $conexio->query($sql);




    while ($row = $resultat->fetch_assoc()){

        $articles[] = $row;

    }

    $response = $app->response();
    $response['Content-Type'] = 'application/json';
    $response->status(200);

    // Generar la resposta en forma d'objecte JSON
    $json = '{"articles":';
    $json .= json_encode($articles);
    $json .= '}';

    $response->body($json);

    // crear una resposta on les dades s'envien en format JSON

    return $response;

})->setParams(array($app));


/* OBTENIR UN ARTICLE CONCRET EN CATALA */ // WORK IN PROGRESS
$app->get('/articles/cat/{id}',function($request) use ($app){

    $id = $request->getAttribute('id'); // obtenir el id del article

    $sql = 'SELECT * FROM articles WHERE idioma_id=1 AND id_article=$id';

    $conexio = obrirConexio();

    $resultat = $conexio->query($sql);



    $response = $app->response();
    $response['Content-Type'] = 'application/json';
    $response->status(200);

    $response->body(json_encode($resultat));

    // crear una resposta on les dades s'envien en format JSON

    return $response;

})->setParams(array($app));


/* OBTENIR TOTS ELS ARTICLES EN CASTELLA */
$app->get('/articles/es/',function() use ($app){

    $sql = "SELECT * FROM articles WHERE idioma_id=2";

    $conexio = obrirConexio();

    $resultat = $conexio->query($sql);




    while ($row = $resultat->fetch_assoc()){

        $articles[] = $row;

    }

    $response = $app->response();
    $response['Content-Type'] = 'application/json';
    $response->status(200);

    // Generar la resposta en forma d'objecte JSON
    $json = '{"articles":';
    $json .= json_encode($articles);
    $json .= '}';

    $response->body($json);

    // crear una resposta on les dades s'envien en format JSON

    return $response;

})->setParams(array($app));


/* OBTENIR UN ARTICLE CONCRET EN CASTELLA */ // WORK IN PROGRESS
$app->get('/articles/es/{id}',function($request) use ($app){

    $id = $request->getAttribute('id'); // obtenir el id del article

    $sql = "SELECT * FROM articles WHERE idioma_id=2 AND id_article=$id";

    $conexio = obrirConexio();

    $resultat = $conexio->query($sql);




    $response = $app->response();
    $response['Content-Type'] = 'application/json';
    $response->status(200);

    $response->body(json_encode($resultat));

    // crear una resposta on les dades s'envien en format JSON

    return $response;

})->setParams(array($app));


/* AFEGIR ARTICLES */  // NO FUNKSIONA
$app->post('/articles/',function($data) use ($app){

    try{
        require_once('bd_connection.php');

        $conexio = obrirConexio();

        $stmt = $conexio->prepare("INSTERT INTO articles (id_article, titol_article, cos_article, idioma_id)
        VALUES (?, ?, ?, ?");

        $stmt->bind_param("ssi", $titol_article, $cos_article, $idioma_id);
        $stmt->execute();




    } catch (Exception $e){
        $error = $e->getMessage();

        echo $error;
    }

})->setParams(array($app));

$app->run();


?>
