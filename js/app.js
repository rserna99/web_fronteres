$(document).ready(function(){
    mostrarInici();
});


function mostrarInici(){
    
    $(".inici").addClass("active");
    $(".qui-som").removeClass("active");
    $(".proxims-esdeveniments").removeClass("active");
    $(".que-fem").removeClass("active");
    $(".articles").removeClass("active");

    var plantilla = $("#plantilla-inici").render();
    
    $("#app").html(plantilla);
}

function mostrarQuiSom(){

    $(".qui-som").addClass("active");
    $(".inici").removeClass("active");
    $(".proxims-esdeveniments").removeClass("active");
    $(".que-fem").removeClass("active");
    $(".articles").removeClass("active");


    var plantilla = $("#plantilla-qui-som").render();
    
    $("#app").html(plantilla);

}

function mostrarEsdeveniments() {
    $(".qui-som").removeClass("active");
    $(".inici").removeClass("active");
    $(".proxims-esdeveniments").addClass("active");
    $(".que-fem").removeClass("active");
    $(".articles").removeClass("active");

    var plantilla = $("#plantilla-esdeveniments").render();

    $("#app").html(plantilla);

}

function mostrarQueFem(){

    $(".que-fem").addClass("active");
    $(".qui-som").removeClass("active");
    $(".inici").removeClass("active");
    $(".proxims-esdeveniments").removeClass("active");
    $(".articles").removeClass("active");


    var plantilla = $("#plantilla-que-fem").render();

    $("#app").html(plantilla);

}

function mostrarArticles(){

    $(".articles").addClass("active");
    $(".qui-som").removeClass("active");
    $(".que-fem").removeClass("active");
    $(".inici").removeClass("active");
    $(".proxims-esdeveniments").removeClass("active");


    $.getJSON("api/index.php/articles/cat", function(data){

        var plantilla = $("#plantilla-articles").render(data);

        console.log(data);

        $("#app").html(plantilla);

    });
}