var root = null;
var useHash = true; // Defaults to: false
var hash = '#!'; // Defaults to: '#'
var router = new Navigo(root, useHash, hash);

router
    .on('/', mostrarInici)
    .on('/inici', mostrarInici)
    .on('/qui-som', mostrarQuiSom)
    .on('/que-fem', mostrarQueFem)
    .on('/articles', mostrarArticles)
    .on('/esdeveniments', mostrarEsdeveniments)
    .resolve();